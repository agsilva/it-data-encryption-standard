/*Author: Rui Pedro Paiva
Teoria da Informação, LEI, 2008/2009*/

/*Modificado por
Ana Rita Pascoa 2010129292
Madalena Fernandinho - 2012135350
*/

#include "DES.h"

#define SUBKEYS 16
//#define DEBUG

//função para encriptação
int DES (char* inFileName, unsigned long long key)
{
	return DESgeneral(inFileName, key, 0);
}

//função para decriptação
int unDES (char* inFileName, unsigned long long key)
{
	return DESgeneral(inFileName, key, 1);
}

//função geral para encriptação (type = 0) e decriptação (type = 1) de um ficheiro
int DESgeneral (char* inFileName, unsigned long long key, int type)
{
	FILE* DESInFile;
	unsigned char* inByteArray;
	long inFileSize;
	unsigned char* crpByteArray;
	char* outFileName;
	int write;
	char response;
	struct stat stFileInfo;
	FILE* DESOutFile;
	char suf[5];

	//abrir ficheiro e ler tamanho
	DESInFile = fopen(inFileName, "rb");
	if (DESInFile == NULL)
	{
		printf("Error opening file for reading. Exiting...\n");
		return 1;
	}
	fseek(DESInFile, 0L, SEEK_END);
	inFileSize = ftell(DESInFile);  //ignore EOF
	fseek(DESInFile, 0L, SEEK_SET);


	//ler ficheiro inteiro para array inByteArray
	inByteArray = (unsigned char*) calloc(inFileSize, sizeof(unsigned char));
	fread(inByteArray, 1, inFileSize, DESInFile);


	//criar assinatura
	if (type == 0)
	{
		//Nao e' pedido
	#ifdef DEBUG
		printf("\nTYPE = 0: Encrypting %s\n",inFileName);
	#endif
	}
	else{
	#ifdef DEBUG
		printf("\n\nTYPE = 1: Decrypting %s\n",inFileName);
	#endif		
	}
	
	//encriptar dados e assinatura no array
	crpByteArray = encryptDES(inByteArray, inFileSize, key, type);

	//nome do ficheiro de saída
	if (type == 0)  //encriptação
	{
		outFileName = (char*) calloc(strlen(inFileName) + 5, sizeof(char));
		strcpy(outFileName, inFileName);
		strcat(outFileName, ".DES");
	}
	else  //decriptação
	{
		strcpy(suf, &inFileName[strlen(inFileName) - 4]);
		if (strcmp(suf, ".DES") == 0)
		{
			outFileName = (char*) calloc(strlen(inFileName) + 5, sizeof(char));
			strcpy(outFileName, "DES_");
			strcat(outFileName, inFileName);
			outFileName[strlen(outFileName) - 4] = 0;
		}
		else
		{
			outFileName = (char*) calloc(14, sizeof(char));
			strcpy(outFileName, "DES_decrypted");
		}

	}

	//verificar assinatura
	if (type == 1)
	{
		//Nao e' pedido
	}

	//criar ficheiro
	write = 1;
	if(stat(outFileName, &stFileInfo) == 0) //see if file already exists
	{
		printf ("File already exists. Overwrite (y/n)?: ");
		response = getchar();
		while(response != 'n' && response != 'y')
			response = getchar();
		if (response == 'n')
			write = 0;
		printf("\n");
		fflush(stdin);
	}

	if (write)
	{
		DESOutFile = fopen(outFileName, "wb");
		if (DESOutFile == NULL)
		{
			printf("Error opening file for writing!!! Exiting...\n");
			return -1;
		}
		fwrite(crpByteArray, 1, inFileSize, DESOutFile);
		fclose(DESOutFile);
	}

	//finalizações
	free(inByteArray);
	free(outFileName);
	free(crpByteArray);
	fclose(DESInFile);

	return 0;
}


// função para encriptação/decriptação de dados no array inByteArray, de dimensão dim
unsigned char* encryptDES(unsigned char* inByteArray, long dim, unsigned long long key, int type)
{
	unsigned long long subKeys[SUBKEYS];
	unsigned char* outByteArray;
	unsigned long long plain, cipher, aux;
	int i, j;


	//obtém sub-keys (16 de comprimento 48)
	DESKeySchedule(key, subKeys);


	if (type == 1) //decrypt --> inverter subKeys
	{
		for(i=0; i<(SUBKEYS/2); i++)
		{
            aux = subKeys[SUBKEYS-1-i];
            subKeys [SUBKEYS-1-i] = subKeys[i];
            subKeys[i] = aux;
		}
	}

	outByteArray = (unsigned char*) calloc(dim, sizeof(unsigned char));
	i = 0;
	plain = 0;

#ifdef DEBUG
	printf("\t\tCipher\n");
#endif
	while (i < dim)
	{
		plain = 0;
		j = i;
		while (j < i + 8 && j < dim)
		{
 			plain = plain | ((unsigned long long)inByteArray[j] << (64 - 8*(j-i+1)));
			j++;
		}

		//determina cifra
		if (j - i == 8)  //ficheiro é múltiplo de 8 bytes
			cipher = encryptDESplain(plain, subKeys);

		else
			cipher = plain;

	#ifdef DEBUG
		printf("%d:\t   %llx\n", i,cipher);
	#endif
		//guarda cifra no array de saída
		j = i;
		while (j < i + 8 && j < dim)
		{
			outByteArray[j] = (unsigned char) (cipher >> (56 - 8*(j-i)) & (0xFF));
			j++;
		}

		i = j;
	}

	return outByteArray;
}


/************************************************************************************/
/***************************** ADICIONAR CÓDIGO *************************************/
/************************************************************************************/
// função para gerar sub-keys (uma chave para cada uma das 16 iterações)
void DESKeySchedule(unsigned long long key, unsigned long long* subKeys)
{
    int PC[] = {57, 49, 41, 33, 25, 17, 9,
                  1, 58, 50, 42, 34, 26, 18,
                 10,  2, 59, 51, 43, 35, 27,
                 19, 11,  3, 60, 52, 44, 36,
                 63, 55, 47, 39, 31, 23, 15,
                  7, 62, 54, 46, 38, 30, 22,
                 14,  6, 61, 53, 45, 37, 29,
                 21, 13,  5, 28, 20, 12, 4};

    int PC2[] = {14, 17, 11, 24,  1, 5,
                  3, 28, 15,  6, 21, 10,
                 23, 19, 12,  4, 26, 8,
                 16,  7, 27, 20, 13, 2,
                 41, 52, 31, 37, 47, 55,
                 30, 40, 51, 45, 33, 48,
                 44, 49, 39, 56, 34, 53,
                 46, 42, 50, 36, 29, 32};

    int i, j, pos;
    int vi[SUBKEYS];
    unsigned long long key_permutada = 0, C0, D0, key_shift = 0, key_concatenada = 0, key_temp = 0;

    //Cria o vector para definir os shifts circulares
    for(i=1; i<=SUBKEYS; i++)
    {
        if( i==1 || i==2 || i==9 || i==16 )
            vi[i-1] = 1;
        else
            vi[i-1] = 2;
    }


    //Permutar com PC1
    for(i=0; i<56; i++)
    {
        pos = 64 - PC[i];
        key_permutada = (key_permutada << 1) | ((key >> pos) & 1);
    }
#ifdef DEBUG
	printf("\n ** Chave: %llx **\n", key);
    printf("Chave permutada com PC1: %llx\n", key_permutada);
#endif

    //Dividir a chave em 2 sequencias de 28bits C0 e D0
    D0 = key_permutada & 0xFFFFFFF; //Para ficar só com 28 e o resto ser zero
    C0 = (key_permutada >> 28 & 0xFFFFFFF);

#ifdef DEBUG
	printf("\n\t   C\t\t   D\t  --->     SubKey\n");
	printf("%d : \t%llx\t\t%llx\n", 0, C0,D0);
#endif

    //Geração das 16 sub-chaves
    for(i=0; i<SUBKEYS; i++)
    {
        for(j=0; j<vi[i]; j++)
        {
            //Tira do menos e mete no mais significativo
            key_shift = (C0 << 1) | ((C0 >> (28-1)) & 1);
            C0 = key_shift & 0xFFFFFFF;

            key_shift = (D0 << 1) | ((D0 >> (28-1)) & 1);
            D0 = key_shift & 0xFFFFFFF;
        }

        //Concatena C0 com D0
        key_concatenada = (C0 << 28) | D0;

        //Permutacao PC2
        key_temp=0;
        for(j=0; j<48; j++) //Percorre PC2
        {
            pos = 56 - PC2[47-j];
            key_temp += (((key_concatenada >> pos) & 0x0000000000000001) << j);
        }

	#ifdef DEBUG
		printf("%d : \t%llx\t\t%llx\t\t%llx\n", i+1, C0,D0,key_temp);
	#endif
        subKeys[i] = key_temp;
    }

}

//função para encriptação de uma mensagem de 64 bits (plain), com base nas subKeys
//devolve a mensagem cifrada
unsigned long long encryptDESplain(unsigned long long plain, unsigned long long* subKeys)
{
    //Tabelas
    int IP[64] = {58, 50, 42, 34, 26, 18, 10, 2,
                  60, 52, 44, 36, 28, 20, 12, 4,
                  62, 54, 46, 38, 30, 22, 14, 6,
                  64, 56, 48, 40, 32, 24, 16, 8,
                  57, 49, 41, 33, 25, 17,  9, 1,
                  59, 51, 43, 35, 27, 19, 11, 3,
                  61, 53, 45, 37, 29, 21, 13, 5,
                  63, 55, 47, 39, 31, 23, 15, 7};

    int inverse_IP[64] = {40, 8, 48, 16, 56, 24, 64, 32,
						  39, 7, 47, 15, 55, 23, 63, 31,
                          38, 6, 46, 14, 54, 22, 62, 30,
                          37, 5, 45, 13, 53, 21, 61, 29,
                          36, 4, 44, 12, 52, 20, 60, 28,
                          35, 3, 43, 11, 51, 19, 59, 27,
                          34, 2, 42, 10, 50, 18, 58, 26,
                          33, 1, 41,  9, 49, 17, 57, 25};

    int E[48] = {32,  1,  2,  3,  4,  5,
				  4,  5,  6,  7,  8,  9,
                  8,  9, 10, 11, 12, 13,
				 12, 13, 14, 15, 16, 17,
				 16, 17, 18, 19, 20, 21,
				 20, 21, 22, 23, 24, 25,
				 24, 25, 26, 27, 28, 29,
				 28, 29, 30, 31, 32,  1};

    int P[32] = {16,  7, 20, 21,
				 29, 12, 28, 17,
				  1, 15, 23, 26,
				  5, 18, 31, 10,
                  2,  8, 24, 14,
                 32, 27,  3,  9,
                 19, 13, 30,  6,
                 22, 11,  4, 25};

    int S [8][4][16] = {
    {{14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7},
    {  0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8},
    {  4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0},
    { 15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13}},

    {{15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10},
    {  3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5},
    {  0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15},
    { 13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9}},

    {{10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8},
    { 13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1},
    { 13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7},
    {  1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12}},

    {{ 7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15},
    { 13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9},
    { 10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4},
    {  3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14}},

    {{ 2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9},
    { 14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6},
    {  4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14},
    { 11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3}},

    {{12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11},
    { 10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8},
    {  9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6},
    {  4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13}},

    {{ 4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1},
    { 13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6},
    {  1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2},
    {  6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12}},

    {{13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7},
    {  1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2},
    {  7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8},
    {  2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11}}};


    unsigned long long cipher_msg = 0, L, R, Li, Ri, expansao = 0, novaPlain = plain, cipher_final = 0;
    int blocoSeis, colunaS, linhaS, b1, b6, b2345, pos, i, j;

    //Permutação IP
    for(i=0; i<64; i++)
    {
        pos = 64 - IP[i];
        cipher_msg = (cipher_msg << 1) | ((plain >> pos) & 1);
    }
	
	//printf("Chave: %llx\n", cipher_msg);

    //Direita e esquerda
    Ri = cipher_msg & 0xFFFFFFFF; //Para ficar só com 32 e o resto ser zero
    Li = ((cipher_msg >> 32) & 0xFFFFFFFF);

    //printf("R0: %llx", Ri);
    //printf("\t\tL0: %llx\n", Li);

    for(i=0; i<SUBKEYS; i++)
    {
		expansao = 0;
		cipher_msg = 0;
        //Faz a expansão com a tabela E
        for(j=0; j<48; j++)
        {
            pos = 32 - E[j];
            expansao = (expansao << 1) | ((Ri >> pos) & 1);
        }

        novaPlain = expansao ^ subKeys[i]; //novaPlain = expansao xor subKeys[i]

        //Blocos S
        for(j=1; j<=8; j++)
        {
            blocoSeis = (novaPlain >> (6*(8-j))) & 0x3F; //0x3F = 63 = 0011 1111, ou seja dá os ultimos 24 bits

            b1 = (blocoSeis >> 5) & 1; //b1 = 1º bit
            b6 = blocoSeis & 1; //b6 = 6º bit
            linhaS = (2*b1) + b6; //Número da coluna a ler -> 2*b1 + b6

            //b2345 = 2º a 5º bit
            b2345 = (blocoSeis >> 1) & 0xF; // 0xF = 15 = 0000 1111, ou seja dá os últimos 4 bits
            colunaS = b2345; //Número da linha a ler

            cipher_msg = (cipher_msg << 4) | (S[j-1][linhaS][colunaS] & 0xF); // 0xF = 15 = 0000 1111, ou seja dá os últimos 4 bits
        }

        //novaPlain = f(Ri-1, Ki)
        novaPlain = 0;
        for(j=0; j<32; j++)
        {
            pos = 32 - P[j];
            novaPlain = (novaPlain << 1) | ((cipher_msg >> pos) & 1);
        }

        L = Ri; //Li = Ri-1
        R = Li ^ novaPlain; //Ri = Li-1 xor f(Ri-1, Ki)
        
        //printf ("L = %llX ---- R = %llX\n", L, R);

        Li = L;
        Ri = R;
    }

    //Concatena R com L
    novaPlain = ((R << 32) | (L & 0xFFFFFFFF)) & 0XFFFFFFFFFFFFFFFFLL;

    //Cifra final
    for(j=0; j<64; j++)
    {
        pos = 64 - inverse_IP[j];
        cipher_final = (cipher_final << 1) | ((novaPlain >> pos) & 1);
    }

    return cipher_final;

}

