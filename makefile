CC = gcc
CFLAGS = -Wall

OBJS = main.o DES.o
HEADERS = DES.h

DES: ${OBJS}
	${CC} ${OBJS} -o DES ${CFLAGS}

main.o: main.c ${HEADERS}
	${CC} ${CFLAGS} main.c -c -o main.o
DES.o: DES.c DES.h
	${CC} ${CFLAGS} DES.c -c -o DES.o

clean:
	rm -f main ${OBJS}
	@echo "Executable and objects all removed!"

clear:
	rm -f main ${OBJS}
	@echo "Executable and objects all removed!"
